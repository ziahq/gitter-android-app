package im.gitter.gitter.content;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;

import java.util.HashMap;
import java.util.Map;

import im.gitter.gitter.models.Group;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.models.User;
import im.gitter.gitter.utils.DatabaseUtils;

public class ContentProvider extends android.content.ContentProvider {

    private static final String TAG = ContentProvider.class.getSimpleName();

    private static final String AUTHORITY = "im.gitter.gitter.provider";
    private static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY);

    public static final Uri ROOMS_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "rooms");
    public static final Uri USERS_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "users");
    public static final Uri GROUPS_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "groups");

    public static final Uri USER_ROOMS_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "user_rooms");
    public static final Uri ADMIN_GROUPS_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "admin_groups");
    public static final Uri USER_GROUPS_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "user_groups");
    public static final Uri USER_GROUPS_WITH_UNREADS_CONTENT_URI = Uri.withAppendedPath(BASE_URI, "user_groups_with_unreads");

    private static final int ROOMS = 0;
    private static final int USERS = 1;
    private static final int GROUPS = 2;
    private static final int ROOMS_SLASH_ID = 3;
    private static final int USERS_SLASH_ID = 4;
    private static final int GROUPS_SLASH_ID = 5;

    private static final int USER_ROOMS = 6;
    private static final int ADMIN_GROUPS = 7;
    private static final int USER_GROUPS = 8;
    private static final int USER_GROUPS_WITH_UNREADS = 9;


    private DatabaseHelper databaseHelper;
    private UriMatcher uriMatcher;
    private ContentResolver contentResolver;
    private SparseArray<QueryPerformer> queryPerformers;
    private SparseArray<BulkInserter> bulkInserters;
    private SparseArray<Deleter> deleters;

    @Override
    public boolean onCreate() {
        databaseHelper = new DatabaseHelper(getContext());
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, "rooms", ROOMS);
        uriMatcher.addURI(AUTHORITY, "rooms/*", ROOMS_SLASH_ID);
        uriMatcher.addURI(AUTHORITY, "users", USERS);
        uriMatcher.addURI(AUTHORITY, "users/*", USERS_SLASH_ID);
        uriMatcher.addURI(AUTHORITY, "groups", GROUPS);
        uriMatcher.addURI(AUTHORITY, "groups/*", GROUPS_SLASH_ID);

        uriMatcher.addURI(AUTHORITY, "user_rooms", USER_ROOMS);
        uriMatcher.addURI(AUTHORITY, "admin_groups", ADMIN_GROUPS);
        uriMatcher.addURI(AUTHORITY, "user_groups", USER_GROUPS);
        uriMatcher.addURI(AUTHORITY, "user_groups_with_unreads", USER_GROUPS_WITH_UNREADS);


        queryPerformers = createQueryPerformers();
        bulkInserters = createBulkInserters();
        deleters = createDeleters();

        contentResolver = getContext().getContentResolver();

        return true;
    }

    private SparseArray<QueryPerformer> createQueryPerformers() {
        SparseArray<QueryPerformer> queryPerformers = new SparseArray<>();
        queryPerformers.put(ROOMS_SLASH_ID, new QueryPerformer() {
            @Override
            public Cursor query(@NonNull SQLiteDatabase db, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
                String id = uri.getLastPathSegment();
                Cursor cursor = db.query(DatabaseHelper.ROOMS_TABLE, projection, "_id = ?", new String[] {id}, null, null, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;
            }
        });
        queryPerformers.put(GROUPS_SLASH_ID, new QueryPerformer() {
            @Override
            public Cursor query(@NonNull SQLiteDatabase db, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
                String id = uri.getLastPathSegment();
                Cursor cursor = db.query(DatabaseHelper.GROUPS_TABLE, projection, "_id = ?", new String[] {id}, null, null, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;
            }
        });
        queryPerformers.put(ROOMS, new QueryPerformer() {
            @Override
            public Cursor query(@NonNull SQLiteDatabase db, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
                Cursor cursor = db.query(DatabaseHelper.ROOMS_TABLE, projection, selection, selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;
            }
        });
        queryPerformers.put(USER_ROOMS, new QueryPerformer() {
            @Override
            public Cursor query(@NonNull SQLiteDatabase db, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
                String mySelection = Room._ID + " IN (SELECT " + UserRoom.ROOM_ID + " FROM " + DatabaseHelper.USER_ROOMS_TABLE + ") " +
                        "AND ("+Room.LAST_ACCESS_TIME+" IS NOT NULL OR "+Room.MENTIONS+" > 0 OR "+Room.UNREAD_ITEMS+" > 0)";
                String mySortOrder =
                        Room.MENTIONS + " DESC, " +
                        Room.UNREAD_ITEMS + " DESC, " +
                        Room.LAST_ACCESS_TIME + " DESC";
                Cursor cursor = db.query(DatabaseHelper.ROOMS_TABLE, projection, mySelection, selectionArgs, null, null, mySortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;
            }
        });
        queryPerformers.put(USERS_SLASH_ID, new QueryPerformer() {
            @Override
            public Cursor query(@NonNull SQLiteDatabase db, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
                String id = uri.getLastPathSegment();
                Cursor cursor = db.query(DatabaseHelper.USERS_TABLE, projection, "_id = ?", new String[] {id}, null, null, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;
            }
        });
        queryPerformers.put(ADMIN_GROUPS, new QueryPerformer() {
            @Override
            public Cursor query(@NonNull SQLiteDatabase db, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
                String mySelection = Group._ID + " IN (SELECT " + AdminGroup.GROUP_ID + " FROM " + DatabaseHelper.ADMIN_GROUPS_TABLE + ")";
                Cursor cursor = db.query(DatabaseHelper.GROUPS_TABLE, projection, mySelection, selectionArgs, null, null, sortOrder);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;
            }
        });
        queryPerformers.put(USER_GROUPS_WITH_UNREADS, new QueryPerformer() {
            @Override
            public Cursor query(@NonNull SQLiteDatabase db, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
                String query = "Select *," +
                        "(SELECT count(*) FROM Rooms r WHERE r.groupId = g._id AND unreadItems > 0) AS unreadRooms, " +
                        "(SELECT count(*) FROM Rooms r WHERE r.groupId = g._id AND mentions > 0) AS mentionedRooms, " +
                        "(SELECT max(lastAccessTime) FROM Rooms r WHERE r.groupId = g._id) AS lastAccessTime " +
                        "FROM Groups g " +
                        "WHERE g._id IN (SELECT " + UserGroup.GROUP_ID + " FROM " + DatabaseHelper.USER_GROUPS_TABLE + ") " +
                        "ORDER BY unreadRooms DESC, mentionedRooms DESC, lastAccessTime DESC";
                Cursor cursor = db.rawQuery(query, null);
                cursor.setNotificationUri(contentResolver, uri);
                return cursor;
            }
        });

        return queryPerformers;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        QueryPerformer performer = queryPerformers.get(uriMatcher.match(uri));

        if (performer != null) {
            SQLiteDatabase db = databaseHelper.getReadableDatabase();
            return performer.query(db, uri, projection, selection, selectionArgs, sortOrder);
        } else {
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }

    private SparseArray<BulkInserter> createBulkInserters() {
        SparseArray<BulkInserter> bulkInserters = new SparseArray<>();
        bulkInserters.put(ROOMS, new BulkInserter() {
            @Override
            public void bulkInsert(@NonNull SQLiteDatabase db, @NonNull Map<String, ContentValues> contentValuesById) {
                DatabaseUtils.upsert(db, DatabaseHelper.ROOMS_TABLE, contentValuesById);
                for (String roomId : contentValuesById.keySet()) {
                    contentResolver.notifyChange(Uri.withAppendedPath(ROOMS_CONTENT_URI, roomId), null);
                }

                // any of the rooms could have affected the following:
                contentResolver.notifyChange(USER_ROOMS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_WITH_UNREADS_CONTENT_URI, null);
            }
        });
        bulkInserters.put(USERS, new BulkInserter() {
            @Override
            public void bulkInsert(@NonNull SQLiteDatabase db, @NonNull Map<String, ContentValues> contentValuesById) {
                DatabaseUtils.upsert(db, DatabaseHelper.USERS_TABLE, contentValuesById);
                for (String userId : contentValuesById.keySet()) {
                    contentResolver.notifyChange(Uri.withAppendedPath(USERS_CONTENT_URI, userId), null);
                }
            }
        });
        bulkInserters.put(GROUPS, new BulkInserter() {
            @Override
            public void bulkInsert(@NonNull SQLiteDatabase db, @NonNull Map<String, ContentValues> contentValuesById) {
                DatabaseUtils.upsert(db, DatabaseHelper.GROUPS_TABLE, contentValuesById);
                for (String groupId : contentValuesById.keySet()) {
                    contentResolver.notifyChange(Uri.withAppendedPath(GROUPS_CONTENT_URI, groupId), null);
                }

                // any of the groups could have affected the following:
                contentResolver.notifyChange(ADMIN_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_WITH_UNREADS_CONTENT_URI, null);
            }
        });
        bulkInserters.put(USER_ROOMS, new BulkInserter() {
            @Override
            public void bulkInsert(@NonNull SQLiteDatabase db, @NonNull Map<String, ContentValues> contentValuesById) {
                db.beginTransaction();
                try {
                    DatabaseUtils.upsert(db, DatabaseHelper.ROOMS_TABLE, contentValuesById);
                    DatabaseUtils.replaceLookups(db, DatabaseHelper.USER_ROOMS_TABLE, UserRoom.ROOM_ID, contentValuesById.keySet());
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                for (String roomId : contentValuesById.keySet()) {
                    contentResolver.notifyChange(Uri.withAppendedPath(ROOMS_CONTENT_URI, roomId), null);
                }

                contentResolver.notifyChange(USER_ROOMS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_WITH_UNREADS_CONTENT_URI, null);
            }
        });
        bulkInserters.put(ADMIN_GROUPS, new BulkInserter() {
            @Override
            public void bulkInsert(@NonNull SQLiteDatabase db, @NonNull Map<String, ContentValues> contentValuesById) {
                db.beginTransaction();
                try {
                    DatabaseUtils.upsert(db, DatabaseHelper.GROUPS_TABLE, contentValuesById);
                    DatabaseUtils.replaceLookups(db, DatabaseHelper.ADMIN_GROUPS_TABLE, AdminGroup.GROUP_ID, contentValuesById.keySet());
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                for (String groupId : contentValuesById.keySet()) {
                    contentResolver.notifyChange(Uri.withAppendedPath(GROUPS_CONTENT_URI, groupId), null);
                }
                contentResolver.notifyChange(ADMIN_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_WITH_UNREADS_CONTENT_URI, null);
            }
        });
        bulkInserters.put(USER_GROUPS, new BulkInserter() {
            @Override
            public void bulkInsert(@NonNull SQLiteDatabase db, @NonNull Map<String, ContentValues> contentValuesById) {
                db.beginTransaction();
                try {
                    DatabaseUtils.upsert(db, DatabaseHelper.GROUPS_TABLE, contentValuesById);
                    DatabaseUtils.replaceLookups(db, DatabaseHelper.USER_GROUPS_TABLE, UserGroup.GROUP_ID, contentValuesById.keySet());
                    db.setTransactionSuccessful();
                } finally {
                    db.endTransaction();
                }
                for (String groupId : contentValuesById.keySet()) {
                    contentResolver.notifyChange(Uri.withAppendedPath(GROUPS_CONTENT_URI, groupId), null);
                }
                contentResolver.notifyChange(ADMIN_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_WITH_UNREADS_CONTENT_URI, null);
            }
        });
        return bulkInserters;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        BulkInserter bulkInserter = bulkInserters.get(uriMatcher.match(uri));
        if (bulkInserter != null) {
            Map<String, ContentValues> contentValuesById = mapById(values);
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            bulkInserter.bulkInsert(db, contentValuesById);
            return contentValuesById.size();
        } else {
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    private SparseArray<Deleter> createDeleters() {
        SparseArray<Deleter> deleters = new SparseArray<>();
        deleters.put(ROOMS, new Deleter() {
            @Override
            public int delete(@NonNull SQLiteDatabase database, String selection, String[] selectionArgs) {
                int rowsAffected = database.delete(DatabaseHelper.ROOMS_TABLE, selection, selectionArgs);
                if (rowsAffected > 0) {
                    contentResolver.notifyChange(ROOMS_CONTENT_URI, null);
                    // any of the rooms could have affected the following:
                    contentResolver.notifyChange(USER_ROOMS_CONTENT_URI, null);
                    contentResolver.notifyChange(USER_GROUPS_WITH_UNREADS_CONTENT_URI, null);
                }
                return rowsAffected;
            }
        });
        deleters.put(USERS, new Deleter() {
            @Override
            public int delete(@NonNull SQLiteDatabase database, String selection, String[] selectionArgs) {
                int rowsAffected = database.delete(DatabaseHelper.ROOMS_TABLE, selection, selectionArgs);
                if (rowsAffected > 0) {
                    contentResolver.notifyChange(USERS_CONTENT_URI, null);
                }
                return rowsAffected;
            }
        });
        deleters.put(GROUPS, new Deleter() {
            @Override
            public int delete(@NonNull SQLiteDatabase database, String selection, String[] selectionArgs) {
                int rowsAffected = database.delete(DatabaseHelper.ROOMS_TABLE, selection, selectionArgs);
                if (rowsAffected > 0) {
                    contentResolver.notifyChange(GROUPS_CONTENT_URI, null);
                }
                // any of the groups could have affected the following:
                contentResolver.notifyChange(ADMIN_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_CONTENT_URI, null);
                contentResolver.notifyChange(USER_GROUPS_WITH_UNREADS_CONTENT_URI, null);
                return rowsAffected;
            }
        });
        deleters.put(USER_ROOMS, new Deleter() {
            @Override
            public int delete(@NonNull SQLiteDatabase database, String selection, String[] selectionArgs) {
                int rowsAffected = database.delete(DatabaseHelper.USER_ROOMS_TABLE, selection, selectionArgs);
                if (rowsAffected > 0) {
                    contentResolver.notifyChange(USER_ROOMS_CONTENT_URI, null);
                }
                return rowsAffected;
            }
        });
        deleters.put(USER_GROUPS, new Deleter() {
            @Override
            public int delete(@NonNull SQLiteDatabase database, String selection, String[] selectionArgs) {
                int rowsAffected = database.delete(DatabaseHelper.USER_GROUPS_TABLE, selection, selectionArgs);
                if (rowsAffected > 0) {
                    contentResolver.notifyChange(USER_GROUPS_CONTENT_URI, null);
                }
                return rowsAffected;
            }
        });
        deleters.put(ADMIN_GROUPS, new Deleter() {
            @Override
            public int delete(@NonNull SQLiteDatabase database, String selection, String[] selectionArgs) {
                int rowsAffected = database.delete(DatabaseHelper.ADMIN_GROUPS_TABLE, selection, selectionArgs);
                if (rowsAffected > 0) {
                    contentResolver.notifyChange(ADMIN_GROUPS_CONTENT_URI, null);
                }
                return rowsAffected;
            }
        });

        return deleters;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        Deleter deleter = deleters.get(uriMatcher.match(uri));
        if (deleter != null) {
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            return deleter.delete(db, selection, selectionArgs);
        } else {
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }

    private Map<String, ContentValues> mapById(@NonNull ContentValues[] valuesArray) {
        Map<String, ContentValues> map = new HashMap<>(valuesArray.length);

        for (ContentValues values : valuesArray) {
            map.put(values.getAsString("_id"), values);
        }

        return map;
    }

    private interface QueryPerformer {
        Cursor query(@NonNull SQLiteDatabase database, @NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder);
    }

    private interface BulkInserter {
        void bulkInsert(@NonNull SQLiteDatabase database, @NonNull Map<String, ContentValues> contentValuesById);
    }

    private interface Deleter {
        int delete(@NonNull SQLiteDatabase database, String selection, String[] selectionArgs);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        final static String ROOMS_TABLE = "Rooms";
        final static String USER_ROOMS_TABLE = "UserRooms";
        final static String USERS_TABLE = "Users";
        final static String GROUPS_TABLE = "Groups";
        final static String ADMIN_GROUPS_TABLE = "AdminGroups";
        final static String USER_GROUPS_TABLE = "UserGroups";

        private final static int DB_VERSION = 3;

        DatabaseHelper(Context context){
            super(context, "Gitter", null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + USERS_TABLE + " (" +
                    User._ID + " TEXT PRIMARY KEY," +
                    User.USERNAME + " TEXT NOT NULL," +
                    User.DISPLAY_NAME + " TEXT NOT NULL," +
                    User.AVATAR_URL + " TEXT NOT NULL" +
                    ");");
            db.execSQL("CREATE TABLE " + GROUPS_TABLE + " (" +
                    Group._ID + " TEXT PRIMARY KEY," +
                    Group.URI + " TEXT NOT NULL," +
                    Group.NAME + " TEXT NOT NULL," +
                    Group.AVATAR_URL + " TEXT NOT NULL," +
                    Group.BACKED_BY_TYPE + " TEXT," +
                    Group.BACKED_BY_LINK_PATH + " TEXT" +
                    ");");
            db.execSQL("CREATE TABLE " + ROOMS_TABLE + " (" +
                    Room._ID + " TEXT PRIMARY KEY," +
                    Room.NAME + " TEXT NOT NULL," +
                    Room.URI + " TEXT NOT NULL," +
                    Room.URL + " TEXT NOT NULL," +
                    Room.AVATAR_URL + " TEXT NOT NULL," +
                    Room.TOPIC + " TEXT," +
                    Room.ONE_TO_ONE + " BOOLEAN NOT NULL," +
                    Room.ACTIVITY + " BOOLEAN NOT NULL," +
                    Room.LURK + " BOOLEAN NOT NULL," +
                    Room.UNREAD_ITEMS + " TEXT NOT NULL," +
                    Room.MENTIONS + " TEXT NOT NULL," +
                    Room.ROOM_MEMBER + " BOOLEAN NOT NULL," +
                    Room.LAST_ACCESS_TIME + " INTEGER," +
                    Room.USER_ID + " TEXT," +
                    Room.GROUP_ID + " TEXT," +
                    "FOREIGN KEY(" + Room.USER_ID + ") REFERENCES " + USERS_TABLE + "(" + User._ID + ")," +
                    "FOREIGN KEY(" + Room.GROUP_ID + ") REFERENCES " + GROUPS_TABLE + "(" + Group._ID + ")" +
                    ");");
            db.execSQL("CREATE TABLE " + USER_ROOMS_TABLE + " (" +
                    UserRoom.ID + " INTEGER PRIMARY KEY," +
                    UserRoom.ROOM_ID + " TEXT UNIQUE," +
                    "FOREIGN KEY(" + UserRoom.ROOM_ID + ") REFERENCES " + ROOMS_TABLE + "(" + Room._ID + ")" +
                    ");");
            db.execSQL("CREATE TABLE " + ADMIN_GROUPS_TABLE + " (" +
                    AdminGroup.ID + " INTEGER PRIMARY KEY," +
                    AdminGroup.GROUP_ID + " TEXT UNIQUE," +
                    "FOREIGN KEY(" + AdminGroup.GROUP_ID + ") REFERENCES " + GROUPS_TABLE + "(" + Group._ID + ")" +
                    ");");
            db.execSQL("CREATE TABLE " + USER_GROUPS_TABLE + " (" +
                    UserGroup.ID + " INTEGER PRIMARY KEY," +
                    UserGroup.GROUP_ID + " TEXT UNIQUE," +
                    "FOREIGN KEY(" + UserGroup.GROUP_ID + ") REFERENCES " + GROUPS_TABLE + "(" + Group._ID + ")" +
                    ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.i(TAG, "dropping database as upgrading " + oldVersion + " -> " + newVersion);
            db.execSQL("DROP TABLE IF EXISTS " + USER_ROOMS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + USER_GROUPS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + ADMIN_GROUPS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + ROOMS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + USERS_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + GROUPS_TABLE);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }

    private static final class UserRoom {
        static final String ID = "_id";
        static final String ROOM_ID = "roomId";
    }

    private static final class AdminGroup {
        static final String ID = "_id";
        static final String GROUP_ID = "groupId";
    }

    private static final class UserGroup {
        static final String ID = "_id";
        static final String GROUP_ID = "groupId";
    }
}
