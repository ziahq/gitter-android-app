package im.gitter.gitter.notifications;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import im.gitter.gitter.content.RestServiceHelper;

public class NotificationReceiver extends BroadcastReceiver {

    private static final String TAG = NotificationReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Notification Received");

        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty() && GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
            String roomId = extras.getString("id");

            // preload the room in the database
            RestServiceHelper.getInstance().getRoom(context, roomId);

            final PendingResult result = goAsync();
            new RoomNotifications(context, roomId).update(new RoomNotifications.Listener() {
                @Override
                public void onFinish() {
                    result.setResultCode(Activity.RESULT_OK);
                    result.finish();
                }
            });
        }
    }
}
