package im.gitter.gitter.models;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Group {

    static final String GH_ORG = "GH_ORG";
    static final String GH_USER = "GH_USER";
    static final String GH_REPO = "GH_REPO";

    public static final String _ID = "_id";
    public static final String URI = "uri";
    public static final String NAME = "name";
    public static final String AVATAR_URL = "avatarUrl";
    public static final String BACKED_BY_TYPE = "backedBy_type";
    public static final String BACKED_BY_LINK_PATH = "backedBy_linkPath";

    @NonNull private final String id;
    @NonNull private final String uri;
    @NonNull private final String name;
    @NonNull private final String avatarUrl;
    @Nullable private final String backedBy_type;
    @Nullable private final String backedBy_linkPath;

    public Group(
            @NonNull String id,
            @NonNull String uri,
            @NonNull String name,
            @NonNull String avatarUrl,
            @Nullable String backedBy_type,
            @Nullable String backedBy_linkPath) {
        this.id = id;
        this.uri = uri;
        this.name = name;
        this.avatarUrl = avatarUrl;
        this.backedBy_type = backedBy_type;
        this.backedBy_linkPath = backedBy_linkPath;
    }

    public String getId() {
        return id;
    }

    public String getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    @Nullable
    public String getBackedBy_type() {
        return backedBy_type;
    }

    @Nullable
    public String getBackedBy_linkPath() {
        return backedBy_linkPath;
    }

    public String getGithubGroupName() {
        if (GH_ORG.equals(backedBy_type) || GH_USER.equals(backedBy_type)) {
            return backedBy_linkPath;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return name;
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(_ID, id);
        contentValues.put(URI, uri);
        contentValues.put(NAME, name);
        contentValues.put(AVATAR_URL, avatarUrl);
        contentValues.put(BACKED_BY_TYPE, backedBy_type);
        contentValues.put(BACKED_BY_LINK_PATH, backedBy_linkPath);
        return contentValues;
    }

    public static Group newInstance(ContentValues contentValues) {
        return new Group(
                contentValues.getAsString(_ID),
                contentValues.getAsString(URI),
                contentValues.getAsString(NAME),
                contentValues.getAsString(AVATAR_URL),
                contentValues.getAsString(BACKED_BY_TYPE),
                contentValues.getAsString(BACKED_BY_LINK_PATH)
        );
    }
}